#include <iostream>
#include <vector>

int main()
{
    std::vector<int> numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::vector<int>::const_iterator iter;
    for (iter = numbers.begin(); iter != numbers.end(); ++iter)
        std::cout << *iter << '\t';
    return 0;
}
